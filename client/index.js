import autoBind from 'auto-bind';
import onChange from 'on-change';
import { createEl, addEl } from 'lmnt';

import Menu from './modules/menu';
import Game from './modules/game';
import Share from './modules/share';
import Instructions from './modules/instructions';


import './index.scss';

class App {
  constructor() {
    autoBind(this);

    const state = {
      view: null,
      guesses: 0,
      guess: null,
      loggedIn: false,
      phrase: null,
    };

    this.state = onChange(state, this.update);

    this.el = createEl('div', { className: 'app' });
    addEl(this.el);

    this.game = new Game(this.state);
    this.menu = new Menu(this.state);
    this.share = new Share(this.state);
    this.instructions = new Instructions(this.state);

    this.views = {
      game: this.game,
      instructions: this.instructions,
      share: this.share,
    };

    addEl(this.el, this.game.el, this.share.el, this.instructions.el, this.menu.el);

    this.state.view = 'game';
    // addEl(this.el, this.game.el);
  }


  update(path, current, previous) {
    console.log(`${path}: ${previous} -> ${current}`);

    if (path === 'view') {
      if (current) this.views[current].show();
      if (previous) this.views[previous].hide();
    }

    if (path === 'guess' && current) {
      this.game.guess();
    }
  }
}

const app = new App();
if (window.location.hostname === 'localhost') window.app = app;
