import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Menu {
  constructor(state) {
    autoBind(this);
    this.state = state;

    this.el = createEl('div', { className: 'menu' });

    this.start = createEl('button', { innerText: 'Start' }, {}, { click: () => { this.state.view = 'game'; } });
    this.share = createEl('button', { innerText: 'Share' }, {}, { click: () => { this.state.view = 'share'; } });
    this.instructions = createEl('button', { innerText: 'Instructions' }, {}, { click: () => { this.state.view = 'instructions'; } });
    this.suggestions = createEl('button', { innerText: 'Suggest a Phrase' }, {}, { click: () => { this.state.view = 'suggestion'; } });

    addEl(this.el, this.start, this.share, this.instructions, this.suggestions);
  }
}
