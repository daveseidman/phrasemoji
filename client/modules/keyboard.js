import { createEl, addEl } from 'lmnt';

const keys = {
  q: {},
  w: {},
  e: {},
  r: {},
  t: {},
  y: {},
  u: {},
  i: {},
  o: {},
  p: {},
  newline1: {},
  pad1: {},
  a: {},
  s: {},
  d: {},
  f: {},
  g: {},
  h: {},
  j: {},
  k: {},
  l: {},
  pad2: {},
  newline2: {},
  enter: {},
  z: {},
  x: {},
  c: {},
  v: {},
  b: {},
  n: {},
  m: {},
  back: {},
  newline3: {},
  space: {},
};

const doubleTapSpeed = 500;

export default class Keyboard {
  constructor() {
    this.el = createEl('div', { className: 'keyboard' });

    this.keys = createEl('div', { className: 'keyboard-keys' });

    addEl(this.el, this.keys);

    Object.keys(keys).forEach((key) => {
      keys[key].el = createEl('div', { className: 'keyboard-keys-key', innerText: key.toUpperCase() }, { key }, {
        touchstart: (e) => { this.el.dispatchEvent(new CustomEvent('press', { detail: e.target.getAttribute('key') })); },
      });
      addEl(this.keys, keys[key].el);
    });


    // Prevent double-tap zoom:
    this.lastTouch = {
      time: null,
      place: { x: null, y: null },
    };
    this.el.addEventListener('touchstart', (e) => {
      if (e.changedTouches > 1) return;

      if (this.lastTouch.time && e.timeStamp - this.lastTouch.time < doubleTapSpeed) {
        e.preventDefault();
      }

      this.lastTouch.time = e.timeStamp;
      this.lastTouch.place.x = e.changedTouches[0].clientX;
      this.lastTouch.place.y = e.changedTouches[0].clientY;
    });
  }
}
