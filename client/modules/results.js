// this is the results page which offers native OS sharing
import autoBind from 'auto-bind';
import { createEl, addEl } from 'lmnt';

export default class Results {
  constructor(state) {
    this.state = state;
    autoBind(this);

    this.el = createEl('div', { className: 'game-results' });
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }
}
