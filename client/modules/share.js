// This is the generic sharing view

import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Share {
  constructor(state) {
    autoBind(this);
    this.state = state;

    this.el = createEl('div', { className: `share ${this.state.view === 'share' ? '' : 'hidden'}` });

    this.link = createEl('a', { href: 'http://phrasemoji.com', target: '_blank' });
    addEl(this.el, this.link);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }
}
