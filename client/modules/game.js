import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import Emoji from './emoji';
import Input from './input';
import Results from './results';

export default class Game {
  constructor(state) {
    autoBind(this);
    this.state = state;

    this.el = createEl('div', { className: `game ${this.state.view === 'game' ? '' : 'hidden'}` });

    this.emoji = new Emoji(this.state);
    this.input = new Input(this.state);
    this.results = new Results(this.state);

    this.guesses = createEl('div', { className: 'game-guesses' });

    const close = createEl('button', { className: 'close', innerText: '✕' }, {}, { click: () => { this.state.view = null; } });

    addEl(this.el, this.emoji.el, this.input.el, this.guesses, this.results.el, close);
  }

  show() {
    this.el.classList.remove('hidden');
    fetch('/api/getRandomPuzzle').then(res => res.json()).then(({ id, emoji, wordCount }) => {
      this.state.puzzleID = id;
      this.state.emoji = emoji;
      this.state.wordCount = wordCount;
      this.emoji.show();
      this.input.show();
    });
  }

  hide() {
    this.el.classList.add('hidden');
    this.emoji.hide();
    this.input.hide();
  }

  guess() {
    this.state.guesses += 1;
    this.el.classList.add('disabled');
    fetch('/api/guess', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ guess: this.state.guess, id: this.state.puzzleID }),
    }).then(res => res.json()).then(({ result }) => {
      this.input.score(result);
      this.el.classList.remove('disabled');
      const guess = createEl('div', { className: 'game-guesses-guess' });
      const number = createEl('span', { innerText: `${this.state.guesses})` });
      addEl(guess, number);
      this.state.guess.split(' ').forEach((word, index) => {
        const wordEl = createEl('span', { innerText: word, className: result.filter(res => res.position === index).length ? 'correct' : '' });
        addEl(guess, wordEl);
      });

      addEl(this.guesses, guess);
    });
  }
}
