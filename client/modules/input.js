import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import { getTextWidth, isMobile } from './utils';
import Keyboard from './keyboard';

export default class Input {
  constructor(state) {
    autoBind(this);
    this.state = state;

    this.el = createEl('div', { className: 'game-input' });
    this.form = createEl('div', { className: 'game-input-form' });
    this.submit = createEl('button', { className: 'game-input-form-submit', innerText: 'Submit' }, {}, { click: this.validate });
    this.keyboard = new Keyboard();

    this.letterPressed = false;
    this.spacePressed = false;
    this.backPressed = false;
    this.enterPressed = false;

    this.blanks = [];
    addEl(this.el, this.form, this.keyboard.el);

    this.keyboard.el.addEventListener('press', this.update);
    // this.keyboard.el.addEventListener('release', this.releaseKey);
  }

  show() {
    this.el.classList.remove('hidden');
    for (let i = 0; i < this.state.wordCount; i += 1) {
      const blank = createEl('input', { type: 'text' }, { spellcheck: false }, { input: this.update, keydown: this.update });
      if (isMobile()) blank.setAttribute('readonly', true);
      this.blanks.push(blank);
      addEl(this.form, blank);

      this.blanks[0].focus();
    }
    addEl(this.form, this.submit);
  }

  // These should work like next and previousSibling but skip over siblings that are already marked .correct
  nextBlank() {

  }

  prevBlank() {

  }

  update(e) {
    if (e.type === 'keydown') {
      if (e.code === 'Space' && e.target.nextSibling) {
        e.target.nextSibling.focus();
      }
      if (e.code === 'Backspace' && e.target.value === '' && e.target.previousSibling) {
        e.target.previousSibling.focus();
      }
      if (e.code === 'Enter') {
        this.validate();
        // this.state.guess = this.blanks.reduce((prev, cur) => `${prev} ${cur.value}`, '');
      }
    }
    if (e.type === 'input') {
      e.target.value = e.target.value.replaceAll(' ', '').replace(/[^a-z]/gi, '');
      e.target.style.width = `${getTextWidth(e.target)}px`;
    }

    if (e.type === 'press') {
      if (e.detail === 'space') {
        if (document.activeElement.tagName === 'INPUT' && document.activeElement.nextSibling) {
          document.activeElement.nextSibling.focus();
        }
      } else if (e.detail === 'back') {
        if (document.activeElement.tagName === 'INPUT') {
          if (document.activeElement.value.length) {
            document.activeElement.value = document.activeElement.value.slice(0, -1);
            document.activeElement.style.width = `${getTextWidth(document.activeElement)}px`;
          } else if (document.activeElement.previousSibling) {
            document.activeElement.previousSibling.focus();
          }
        }
      } else if (e.detail === 'enter') {
        this.validate();
        // this.state.guess = this.blanks.reduce((prev, cur) => `${prev} ${cur.value}`, '');
      } else {
        if (document.activeElement.tagName === 'INPUT') {
          document.activeElement.value += e.detail;
          document.activeElement.style.width = `${getTextWidth(document.activeElement)}px`;
        }
      }
    }
  }

  validate() {
    const emptyFields = this.blanks.filter(blank => blank.value === '');
    if (emptyFields.length) {
      emptyFields.map(field => field.classList.add('invalid'));
      setTimeout(() => {
        emptyFields.map(field => field.classList.remove('invalid'));
      }, 1000);

      return;
    }
    this.state.guess = this.blanks.reduce((prev, cur) => `${prev} ${cur.value}`, '');
  }

  score(result) {
    console.log(result);
    result.forEach(({ word, position }) => {
      const blank = this.blanks[position - 1];
      blank.value = word;
      blank.classList.add('correct');
      blank.style.width = `${getTextWidth(blank)}px`;
    });
  }

  hide() {
    this.el.classList.add('hidden');
    this.form.innerHTML = '';
    this.blanks = [];
  }
}
