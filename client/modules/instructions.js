import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Instructions {
  constructor(state) {
    autoBind(this);
    this.state = state;

    this.screen = 0;
    this.screenAmount = 3;

    this.el = createEl('div', { className: `instructions ${this.state.view === 'instructions' ? '' : 'hidden'}` });

    const title = createEl('h1', { innerText: 'Instructions' });

    this.carousel = createEl('div', { className: 'instructions-carousel' });


    this.screens = createEl('div', { className: 'instructions-carousel-screens' });

    const screen1 = createEl('div', { className: 'instructions-carousel-screens-screen' });
    const text1 = createEl('p', { innerText: 'Guess the phrase based on the emoji sequence' });
    const img1 = createEl('img', { src: 'assets/test.png' });
    addEl(screen1, text1, img1);

    const screen2 = createEl('div', { className: 'instructions-carousel-screens-screen' });
    const text2 = createEl('p', { innerText: 'Keep guessing until you get it correct' });
    const img2 = createEl('img', { src: 'assets/test.png' });
    addEl(screen2, text2, img2);

    const screen3 = createEl('div', { className: 'instructions-carousel-screens-screen' });
    const text3 = createEl('p', { innerText: 'Share your results on social' });
    const img3 = createEl('img', { src: 'assets/test.png' });
    addEl(screen3, text3, img3);

    this.controls = createEl('div', { className: 'instructions-carousel-controls' });
    this.next = createEl('button', { className: 'instructions-carousel-controls-next', innerText: '→' }, { direction: 1 }, { click: this.changeScreen });
    this.prev = createEl('button', { className: 'instructions-carousel-controls-prev', innerText: '←' }, { direction: -1 }, { click: this.changeScreen });
    addEl(this.controls, this.next, this.prev);

    this.dots = createEl('div', { className: 'instructions-carousel-dots' });
    const dot1 = createEl('span', { className: 'active' });
    const dot2 = createEl('span');
    const dot3 = createEl('span');
    addEl(this.dots, dot1, dot2, dot3);

    addEl(this.screens, screen1, screen2, screen3);

    const close = createEl('button', { className: 'close', innerText: '✕' }, {}, { click: () => { this.state.view = null; } });

    addEl(this.carousel, this.screens, this.controls, this.dots);
    addEl(this.el, title, this.carousel, close);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  changeScreen({ target }) {
    this.screen += parseInt(target.getAttribute('direction'), 10);
    if (this.screen >= this.screenAmount) this.screen = 0;
    if (this.screen < 0) this.screen = this.screenAmount - 1;
    this.screens.style.transform = `translateX(${-this.screen * 100}%)`;

    this.dots.childNodes.forEach((dot, index) => {
      dot.classList[index === this.screen ? 'add' : 'remove']('active');
    });
  }
}
