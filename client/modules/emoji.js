import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import twemoji from 'twemoji';


export default class Emoji {
  constructor(state) {
    autoBind(this);
    this.state = state;

    this.el = createEl('div', { className: 'game-emoji hidden' });
    this.display = createEl('div', { className: 'game-emoji-display' });
    // this.blanks = createEl('div', { className: 'game-emoji-blanks' });
    addEl(this.el, this.display);// , this.blanks);
  }

  show() {
    this.el.classList.remove('hidden');
    this.display.innerText = this.state.emoji;
    twemoji.parse(this.display);
  }

  hide() {
    this.el.classList.add('hidden');
    this.display.innerText = '';
  }
}
