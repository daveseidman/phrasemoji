require('dotenv').config();

const express = require('express');
const path = require('path');

const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const fs = require('fs');
const levenshtein = require('fast-levenshtein');

const pad = (str, len) => {
  str = str.toString();
  while (str.length < len) {
    str = `0${str}`;
  }
  return str;
};

let phrases;
let todaysPhrase = '';

const newPhrase = () => {
  fs.readFile(`${__dirname}/phrases.json`, (err, data) => {
    phrases = JSON.parse(data);
    const date = new Date();
    const today = `${pad(date.getMonth() + 1, 2)}-${pad(date.getDate(), 2)}-${date.getFullYear().toString().slice(-2)}`;
    todaysPhrase = phrases.filter(phrase => phrase.date === today)[0];
  });
};


newPhrase(); // TODO: call this via a timeout or chron

app.use(bodyParser.json());

app.use(express.static(`${__dirname}/../dist`)); // TODO: can probably use a wildcard here

app.get('/api/getTodaysPuzzle', (req, res) => {
  res.send({ id: todaysPhrase.id, emoji: todaysPhrase.emoji, wordCount: todaysPhrase.phrase.split(' ').length });
});

app.get('/api/getRandomPuzzle', (req, res) => {
  const randomPuzzle = phrases[Math.floor(Math.random() * phrases.length)];
  res.send({ id: randomPuzzle.id, emoji: randomPuzzle.emoji, wordCount: randomPuzzle.phrase.split(' ').length });
});

const checkGuess = (guess, correct) => {
  // console.log(guess, todaysPhrase.phrase, levenshtein.get(guess.toLowerCase(), todaysPhrase.phrase.toLowerCase()));
  console.log(guess, correct);
  const guessWords = guess.toLowerCase().split(' ');
  const phraseWords = correct.toLowerCase().split(' ');

  const correctWords = [];
  guessWords.forEach((guessWord, guessIndex) => {
    let matched = false;
    phraseWords.forEach((phraseWord, phraseIndex) => {
      if (!matched && (phraseWord.length - levenshtein.get(guessWord, phraseWord)) / phraseWord.length >= 0.75) {
        matched = true;
        phraseWords.splice(phraseIndex, 1); // remove correct word, only match remaining
        correctWords.push({ word: phraseWord, position: guessIndex });
      }
    });
  });

  return correctWords;
};


app.post('/api/guess', (req, res) => {
  res.send({ result: checkGuess(req.body.guess, phrases.filter(phrase => phrase.id === req.body.id)[0].phrase) });
});


const server = http.listen(process.env.PORT, () => {
  console.log(`Listening on port ${server.address().port}`);
});
