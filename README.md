# Phrasemoji

PWA word game

Players attempt to guess the phrase represented by a series of emoji's.  
Puzzles to be updated daily and become available at midnight each day.  
Players can share their results (how many guesses) or if they gave up.  

## Gameplay

The player is presented with a series of emojis (2 - 10)  
The player types the phrase they believe the series represents  



## Sharing  

Follow mechanic of Wordle (share only score, not answer)  


## Considerations  

Emoji sets have a lot of variation between them so we should use a custom set  that will appear the same on every players computer or device.  


## TODO

- Login flow
- Custom Emoji Font (so that it appears the same across all browsers)  
- leaderboard?
- Analytics
- Input styling
- Sharing
- Suggest a puzzle form  
